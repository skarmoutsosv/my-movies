from flask import render_template, redirect, url_for, request, flash, abort
from flaskMoviesApp.forms import SignupForm, LoginForm, NewMovieForm, AccountUpdateForm
from flaskMoviesApp import app, db, bcrypt
from flaskMoviesApp.models import User, Movie
from flask_login import login_user, current_user, logout_user, login_required

import secrets
from PIL import Image
import os

from datetime import datetime as dt

# Χρησιμοποιούνται μόνο στο route tmdb
import io
import requests
import json
import base64


### Συμπληρώστε κάποια από τα imports που έχουν αφαιρεθεί ###

current_year = dt.now().year


### Μέθοδος μετονομασίας και αποθήκευσης εικόνας ###
def image_save(image, where, size):
    random_filename = secrets.token_hex(8)
    file_name, file_extension = os.path.splitext(image.filename)
    image_filename = random_filename + file_extension
    image_path = os.path.join(app.root_path, 'static/images/' + where, image_filename)

    image_size = size  # this must be a tuple in the form of: (150, 150)
    img = Image.open(image)
    img.thumbnail(image_size)

    img.save(image_path)

    return image_filename


### ERROR HANDLERS START ###

## Εδώ πρέπει να μπει ο κώδικας για τους error handlers (πχ για το error 404 κλπ).
## Θα πρέπει να φτιαχτούν οι error handlers για τα errors 404, 415 και 500.

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404


@app.errorhandler(415)
def unsupported_media_type(e):
    # note that we set the 415 status explicitly
    return render_template('415.html'), 415


@app.errorhandler(500)
def internal_server_error(e):
    # note that we set the 500 status explicitly
    return render_template('500.html'), 500

### ERROR HANDLERS END ###


### Αρχική Σελίδα ###
@app.route("/home/")
@app.route("/")
def root():

    ## Να προστεθεί σε αυτό το view ότι χρειάζεται για την ταξινόμηση
    ## ανά ημερομηνία εισαγωγής στη βάση, ανά έτος προβολής και ανά rating
    ## με σωστή σελιδοποίηση για την κάθε περίπτωση.

    ## Pagination: page value from 'page' parameter from url
    page = request.args.get('page', default=1, type=int)  # type is used to cast the value to a different type, such as int or str while getting it.
    ordering_by = request.args.get('ordering_by')  # Returns either "release_year" or "rating". If key doesn't exist, returns None.

    ## Query για ανάσυρση των ταινιών από τη βάση δεδομένων με το σωστό pagination και ταξινόμηση
    if ordering_by == "release_year":
        movies = Movie.query.order_by(Movie.release_year.desc()).paginate(per_page=5, page=page)
    elif ordering_by == "rating":
        movies = Movie.query.order_by(Movie.rating.desc()).paginate(per_page=5, page=page)
    else:
        movies = Movie.query.order_by(Movie.date_created.desc()).paginate(per_page=5, page=page)

    ## Για σωστή ταξινόμηση ίσως πρέπει να περάσετε κάτι επιπλέον μέσα στο context.
    ## Υπενθύμιση: το context είναι το σύνολο των παραμέτρων που περνάμε
    ##             μέσω της render_template μέσα στα templates μας
    ##             στην παρακάτω περίπτωση το context περιέχει μόνο το movies=movies
    return render_template("index.html", movies=movies, ordering_by=ordering_by)


@app.route("/signup/", methods=['GET', 'POST'])
def signup():

    ## Έλεγχος για το αν ο χρήστης έχει κάνει login ώστε αν έχει κάνει,
    ## να μεταφέρεται στην αρχική σελίδα

    '''Create an instance of the Sign Up form'''
    form = SignupForm(meta={'csrf': False})

    if request.method == 'POST' and form.validate_on_submit():
        ## Να συμπληρωθεί ο κώδικας που φορτώνει τα στοιχεία της φόρμας και τα αποθηκεύει στη βάση δεδομένων
        username = form.username.data
        email = form.email.data
        password = form.password.data
        password2 = form.password2.data
        encrypted_password = bcrypt.generate_password_hash(password).decode('UTF-8')
        user = User(username=username, email=email, password=encrypted_password)
        db.session.add(user)
        db.session.commit()

        flash(f'Ο λογαριασμός για το χρήστη: <b>{username}</b> δημιουργήθηκε με επιτυχία', 'success')

        return redirect(url_for('login_page'))

    return render_template("signup.html", form=form)


## Σελίδα Λογαριασμού Χρήστη με δυνατότητα αλλαγής των στοιχείων του
## Να δοθεί ο σωστός decorator για υποχρεωτικό login
@app.route("/account/", methods=['GET', 'POST'])
@login_required
def account():
    ## Αρχικοποίηση φόρμας με προ-συμπληρωμένα τα στοιχεία του χρήστη
    form = AccountUpdateForm(username=current_user.username, email=current_user.email)

    if request.method == 'POST' and form.validate_on_submit():

        ## Έλεγχος αν έχει δοθεί νέα εικόνα προφίλ, αλλαγή ανάλυσης της εικόνας
        ## και αποθήκευση στον δίσκο του server και στον χρήστη (δηλαδή στη βάση δεδομένων).

        ## Αποθήκευση των υπόλοιπων στοιχείων του χρήστη.

        current_user.username = form.username.data
        current_user.email = form.email.data
        old_image_filename = current_user.profile_image
        if form.profile_image.data:
            try:
                # image_save(image, where, size)
                image_file = image_save(form.profile_image.data, 'profiles_images', (150, 150))
            except:
                abort(415)
            current_user.profile_image = image_file
            # delete old file if it is not the default
            if old_image_filename != 'default_profile_image.png':
                old_image_path = os.path.join(app.root_path, 'static/images/profiles_images', old_image_filename)
                if os.path.exists(old_image_path):
                    os.remove(old_image_path)
                else:
                    print("File does not exist: " + old_image_path)
        db.session.commit()

        flash(f'Ο λογαριασμός του χρήστη: <b>{current_user.username}</b> ενημερώθηκε με επιτυχία', 'success')

        return redirect(url_for('root'))
    else:
        return render_template("account_update.html", form=form)


### Σελίδα Login ###
@app.route("/login/", methods=['GET', 'POST'])
def login_page():

    ## Έλεγχος για το αν ο χρήστης έχει κάνει login ώστε αν έχει κάνει,
    ## να μεταφέρεται στην αρχική σελίδα
    if current_user.is_authenticated:
        return redirect(url_for("root"))

    form = LoginForm()  # Αρχικοποίηση φόρμας Login

    if request.method == 'POST' and form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        ## Ανάκτηση χρήστη από τη βάση με το email του
        ## και έλεγχος του password.
        ## Εάν είναι σωστά, να γίνεται login με τη βοήθεια του Flask-Login
        ## Σε κάθε περίπτωση να εμφανίζονται τα αντίστοιχα flash messages
        user = User.query.filter_by(email=email).first()
        if user and bcrypt.check_password_hash(user.password, password):
            flash(f"Η είσοδος του χρήστη με email: <b>{email}</b> στην σελίδα μας έγινε με επιτυχία", "success")
            login_user(user, remember=form.remember_me.data)
            next_link = request.args.get("next")
            return redirect(next_link) if next_link else redirect(url_for("root"))
        else:
            flash(f"Η είσοδος του χρήστη ήταν ανεπιτυχής, παρακαλούμε δοκιμάστε ξανά με τα σωστά email/password.", category="warning")
    return render_template("login.html", form=form)


### Σελίδα Logout ###
@app.route("/logout/")
def logout_page():
    ## Αποσύνδεση Χρήστη
    logout_user()
    flash('Έγινε αποσύνδεση του χρήστη.', 'success')
    ## Ανακατεύθυνση στην αρχική σελίδα
    return redirect(url_for("root"))


### Σελίδα Εισαγωγής Νέας Ταινίας ###

## Να δοθεί ο σωστός decorator για τη σελίδα με route 'new_movie'
## καθώς και ο decorator για υποχρεωτικό login
@app.route("/new_movie/", methods=["GET", "POST"])
@login_required
def new_movie():
    form = NewMovieForm()

    ## Υλοποίηση της λειτουργίας για ανάκτηση και έλεγχο (validation) των δεδομένων της φόρμας
    ## Τα πεδία που πρέπει να έρχονται είναι τα παρακάτω:
    ## title, plot, image, release_year, rating
    ## Το πεδίο image πρέπει να ελέγχεται αν περιέχει εικόνα και αν ναι
    ## να μετατρέπει την ανάλυσή της σε (640, 640) και να την αποθηκεύει στο δίσκο και τη βάση
    ## αν όχι, να αποθηκεύει τα υπόλοιπα δεδομένα και αντί εικόνας να μπαίνει το default movie image
    if request.method == 'POST' and form.validate_on_submit():
        title = form.title.data
        plot = form.plot.data
        release_year = form.release_year.data
        rating = form.rating.data

        # Αν υπάρχει εικόνα
        if form.image.data:
            try:
                image_file = image_save(form.image.data, 'movies_images', (640, 640))
            except:
                abort(415)
            mymovie = Movie(title=title, plot=plot, author=current_user, image=image_file, release_year=release_year, rating=rating)
        else:
            # Αν δεν υπάρχει εικόνα, αποθήκευση των υπόλοιπων πεδίων
            mymovie = Movie(title=title, plot=plot, author=current_user, release_year=release_year, rating=rating)

        db.session.add(mymovie)
        db.session.commit()

        flash(f'Η ταινία με τίτλο: <b>{title}</b> δημιουργήθηκε με επιτυχία', 'success')

        return redirect(url_for('root'))

    return render_template("new_movie.html", form=form, page_title="Εισαγωγή νέας ταινίας", current_year=current_year)


### Πλήρης σελίδα ταινίας ###

## Να δοθεί ο σωστός decorator για τη σελίδα με route 'movie'
## και επιπλέον να δέχεται το id της ταινίας ('movie_id')
@app.route("/movie/<int:movie_id>", methods=["GET"])
def movie(movie_id):

    ## Ανάκτηση της ταινίας με βάση το movie_id
    ## ή εμφάνιση σελίδας 404 page not found
    mymovie = Movie.query.get_or_404(movie_id)
    return render_template("movie.html", movie=mymovie)


### Ταινίες ανά χρήστη που τις ανέβασε ###

## Να δοθεί ο σωστός decorator για τη σελίδα με route 'movies_by_author'
## και επιπλέον να δέχεται το id του χρήστη ('author_id')
@app.route("/movies_by_author/<int:author_id>")
def movies_by_author(author_id):

    ## Όπως και στην αρχική σελίδα, να προστεθεί σε αυτό το view ότι χρειάζεται για την ταξινόμηση 
    ## ανά ημερομηνία εισαγωγής στη βάση, ανά έτος προβολής και ανά rating
    ## με σωστή σελιδοποίηση για την κάθε περίπτωση.

    ## Pagination: page value from 'page' parameter from url
    page = request.args.get('page', default=1, type=int)
    ordering_by = request.args.get('ordering_by')  # Returns either "release_year" or "rating". If key doesn't exist, returns None.

    # Query για ανάσυρση του χρήστη από τη βάση δεδομένων βάσει του id του ('author_id'), ή εμφάνιση σελίδας 404 page not found
    user = User.query.get_or_404(author_id)

    ## Query για ανάσυρση των ταινιών βάσει χρήστη από τη βάση δεδομένων με το σωστό pagination και ταξινόμηση
    if ordering_by == "release_year":
        movies = Movie.query.filter_by(author=user).order_by(Movie.release_year.desc()).paginate(per_page=3, page=page)
    elif ordering_by == "rating":
        movies = Movie.query.filter_by(author=user).order_by(Movie.rating.desc()).paginate(per_page=3, page=page)
    else:
        movies = Movie.query.filter_by(author=user).order_by(Movie.date_created.desc()).paginate(per_page=3, page=page)

        ## Για σωστή ταξινόμηση ίσως πρέπει να περάσετε κάτι επιπλέον μέσα στο context (εκτός από τον author και τις ταινίες).
        ## Υπενθύμιση: το context είναι το σύνολο των παραμέτρων που περνάμε
        ##             μέσω της render_template μέσα στα templates μας
        ##             στην παρακάτω περίπτωση το context περιέχει τα movies=movies, author=user
    return render_template("movies_by_author.html", movies=movies, author=user, ordering_by=ordering_by)


### Σελίδα Αλλαγής Στοιχείων Ταινίας ###

## Να δοθεί ο σωστός decorator για τη σελίδα με route 'edit_movie'
## και επιπλέον να δέχεται το id της ταινίας προς αλλαγή ('movie_id')
## και να προστεθεί και ο decorator για υποχρεωτικό login
@app.route("/edit_movie/<int:movie_id>", methods=['GET', 'POST'])
@login_required
def edit_movie(movie_id):
    # Ανάκτηση ταινίας βάσει των movie_id, user_id, ή, εμφάνιση σελίδας 404 page not found
    movie = Movie.query.filter_by(id=movie_id, author=current_user).first_or_404()
    # movie = Movie.query.filter_by(id=movie_id, author=current_user).first()

    ## Έλεγχος αν βρέθηκε η ταινία
    ## αν ναι, αρχικοποίηση της φόρμας ώστε τα πεδία να είναι προσυμπληρωμένα
    ## έλεγχος των πεδίων (validation) και αλλαγή (ή προσθήκη εικόνας) στα στοιχεία της ταινίας
    ## αν δε βρέθηκε η ταινία, ανακατεύθυνση στην αρχική σελίδα και αντίστοιχο flash μήνυμα στο χρήστη
    if movie:
        form = NewMovieForm(title=movie.title, plot=movie.plot, release_year=movie.release_year, rating=movie.rating)
        if request.method == 'POST' and form.validate_on_submit():
            movie.title = form.title.data
            movie.plot = form.plot.data
            movie.release_year = form.release_year.data
            movie.rating = form.rating.data

            # Αν υπάρχει εικόνα
            if form.image.data:
                try:
                    image_file = image_save(form.image.data, 'movies_images', (640, 640))
                except:
                    abort(415)
                movie.image = image_file

            db.session.commit()
            flash(f'Η επεξεργασία της ταινίας έγινε με επιτυχία', 'success')
            return redirect(url_for('movie', movie_id=movie.id))
    # Το else δεν εκτελείται ποτέ, γιατί υπάρχει παραπάνω το first_or_404(),
    # Αν όμως χρησιμοποιήσουμε το first(), τότε θα λειτουργήσει όταν μπει στο URI αριθμός ταινίας που δεν υπάρχει
    else:
        flash(f'Δε βρέθηκε η ταινία', 'info')
        return redirect(url_for('root'))
    return render_template("new_movie.html", form=form, movie=movie, page_title="Αλλαγή Ταινίας", current_year=current_year)


### Σελίδα Διαγραφής Ταινίας από τον author της ###

## Να δοθεί ο σωστός decorator για τη σελίδα με route 'delete_movie'
## και επιπλέον να δέχεται το id της ταινίας προς αλλαγή ('movie_id')
## και να προστεθεί και ο decorator για υποχρεωτικό login
@app.route("/delete_movie/<int:movie_id>", methods=["GET", "POST"])
@login_required
def delete_movie(movie_id):
    ## Ανάκτηση ταινίας βάσει των movie_id και author (ο οποίος πρέπει να συμπίπτει με τον current user), ή, εμφάνιση σελίδας 404 page not found
    movie = Movie.query.filter_by(id=movie_id, author=current_user).first_or_404()

    ## Εάν βρεθεί η ταινία, κάνουμε διαγραφή και εμφανίζουμε flash message επιτυχούς διαγραφής
    ## με ανακατεύθυνση στην αρχική σελίδα
    ## αλλιώς εμφανίζουμε flash message ανεπιτυχούς διαγραφής
    ## με ανακατεύθυνση στην αρχική σελίδα
    if movie:
        db.session.delete(movie)
        db.session.commit()
        flash(f'Η ταινία: <b>{movie.title}</b> διαγράφηκε με επιτυχία.', "success")

        delete_remaining_image = True  # True για διαγραφή αρχείου εικόνας από τον δίσκο
        if delete_remaining_image and movie.image != 'default_movie_image.png':
            image_path = os.path.join(app.root_path, 'static/images/movies_images', movie.image)
            if os.path.exists(image_path):
                os.remove(image_path)
            else:
                print("File does not exist: " + image_path)

        return redirect(url_for("root"))
    flash("Η ταινία δεν βρέθηκε.", "warning")
    return redirect(url_for("root"))


# Σελίδα προβολής ταινιών από TMDB
@app.route("/tmdb/")
def tmdb():
    api_addr = "https://api.themoviedb.org/3/discover/movie"
    api_key = "?api_key=6915ec4d6f2dc92a61948492bb0226e8"  # This is API key for SkarmoutsosV TMDB account
    api_params = "&with_original_language=el&language=el&sort_by=popularity.desc&vote_count.gte=30&page="

    # Get values from url GET method
    number = request.args.get('number', default=0, type=int)  # 0-19 from API
    api_page = request.args.get('page', default=1, type=int)  # 1- from API
    action = request.args.get('action')  # Returns "previous", "next" or "save". If key doesn't exist, returns None.
    total_results = request.args.get('total', type=int)  # Χρειάζεται επίσης αν γίνεται αλλαγή των παραμέτρων μέσω UI.

    if total_results is None:
        uri = api_addr + api_key + api_params + str(api_page)  # Δημιουργία API URI
        # Επικοινωνία με API και λήψη απόκρισης
        # https://www.w3schools.com/python/ref_requests_response.asp
        try:
            uri_response = requests.get(uri)
            json_response = uri_response.text
            data = json.loads(json_response)  # Δημιουργία λεξικού που περιέχει τα δεδομένα της απόκρισης
            total_results = data.get('total_results')
        except requests.exceptions.RequestException as e:
            flash(f"Υπήρξε κάποιο πρόβλημα στην επικοινωνία με το TMDB API.", 'danger')
            return render_template("tmdb.html")

    # Έλεγχος των εντολών του χρήστη και δημιουργία του κατάλληλου number, api_page
    if action == 'next':
        if (api_page-1) * 20 + number+1 < total_results:  # Αν τα τρέχοντα αποτελέσματα είναι λιγότερα από τα συνολικά
            if number < 19:
                number = number + 1
            else:
                number = 0
                api_page = api_page + 1
        else:
            # Φτάσαμε στο τελευταίο αποτέλεσμα
            pass

    if action == 'previous':
        if (api_page-1) * 20 + number+1 > 1:  # Αν τα τρέχοντα αποτελέσματα είναι μετά το πρώτο
            if number > 0:
                number = number - 1
            else:
                # Αν έχουμε το πρώτο αποτέλεσμα, δηλαδή αυτό με number=0, τότε
                if api_page > 1:
                    number = 19
                    api_page = api_page - 1
        else:
            # Φτάσαμε στον πρώτο αποτέλεσμα
            pass

    uri = api_addr + api_key + api_params + str(api_page)  # Δημιουργία API URI

    # Επικοινωνία με API και λήψη απόκρισης
    # https://www.w3schools.com/python/ref_requests_response.asp
    try:
        uri_response = requests.get(uri)
    except requests.exceptions.RequestException as e:
        flash(f"Υπήρξε κάποιο πρόβλημα στην επικοινωνία με το TMDB API.", 'danger')
        return render_template("tmdb.html")

    json_response = uri_response.text
    data = json.loads(json_response)  # Δημιουργία λεξικού που περιέχει τα δεδομένα της απόκρισης

    page = data.get('page')
    total_pages = data.get('total_pages')
    total_results = data.get('total_results')

    results = data.get('results')  # Δημιουργία λίστας από λεξικά, όπου το κάθε στοιχείο της λίστας είναι και μία ταινία

    mymovie = results[number]  # Παίρνουμε μία ταινία από τη λίστα και δημιουργούμε το λεξικό mymovie

    original_title = mymovie.get('original_title')
    overview = mymovie.get('overview')
    release_year = mymovie.get('release_date')[:4]
    rating = int(mymovie.get('vote_average') * 10)

    poster_path = mymovie.get('poster_path')
    poster_uri = "https://image.tmdb.org/t/p/w500" + poster_path
    tmdb_movie = Movie(title=original_title, plot=overview, release_year=release_year, rating=rating, image=poster_uri)

    use_file_object_for_image = True  # True αν θέλουμε ο client να παίρνει την εικόνα μέσω του flask server
    if action == 'save' or use_file_object_for_image:
        decoded_img_data = None
        # Λήψη των byte της εικόνας, δημιουργία ενός file object στη μνήμη,
        # αποθήκευση των byte της εικόνας στο file object, πέρασμα του file object στο template.
        try:
            response_obj = requests.get(poster_uri)
            if response_obj.ok:  # To response_obj.ok είναι True όταν το status_code είναι <400
                image_bytes = response_obj.content  # Τα περιεχόμενα της απάντησης σε bytes

                if action == 'save' and current_user.is_authenticated:
                    # Αποθήκευση εικόνας στον δίσκο
                    if io.BytesIO(image_bytes):  # Αν υπάρχει εικόνα
                        try:
                            random_filename = secrets.token_hex(8)
                            file_name, file_extension = os.path.splitext(poster_path)
                            image_filename = random_filename + file_extension
                            image_path = os.path.join(app.root_path, 'static/images/movies_images', image_filename)
                            image_size = (640, 640)  # this must be a tuple in the form of: (150, 150)
                            img = Image.open(io.BytesIO(image_bytes))
                            img.thumbnail(image_size)
                            img.save(image_path)
                        except:
                            abort(415)
                        mymovie = Movie(title=original_title, plot=overview, author=current_user, image=image_filename,
                                           release_year=release_year, rating=rating)
                    else:
                        # Αν δεν υπάρχει εικόνα, αποθήκευση των υπόλοιπων πεδίων
                        mymovie = Movie(title=original_title, plot=overview, author=current_user,
                                           release_year=release_year, rating=rating)
                    db.session.add(mymovie)
                    db.session.commit()
                    flash(f'Η ταινία με τίτλο: <b>{original_title}</b> αποθηκεύτηκε στην συλλογή σας με επιτυχία', 'success')

                if use_file_object_for_image:
                    img_obj = Image.open(io.BytesIO(image_bytes))  # Δημιουργία αντικειμένου από τα ληφθέντα bytes
                    # decoded_img_data = base64.b64encode(io.BytesIO(image_bytes).getvalue()).decode('UTF-8')
                    if use_file_object_for_image and img_obj.format == 'JPEG':
                        file_object = io.BytesIO()  # Δημιουργία file-object στη μνήμη
                        img_obj.save(file_object, 'JPEG')  # write JPEG in file-object
                        encoded_img_data = base64.b64encode(file_object.getvalue())
                        decoded_img_data = encoded_img_data.decode('UTF-8')
        except requests.exceptions.RequestException as e:
            flash(f"Υπήρξε κάποιο πρόβλημα κατά τη λήψη της εικόνας από το TMDB.", 'danger')
            return render_template("tmdb.html")

    print("number:", number, "page:" + str(page), " total_pages:" + str(total_pages), " total_results:" + str(total_results))
    print(tmdb_movie.title, tmdb_movie.release_year, tmdb_movie.rating, tmdb_movie.image)
    api_params = api_params[1:-6].replace("&", ", ")
    return render_template("tmdb.html", tmdb_movie=tmdb_movie, img_data=decoded_img_data,
                           number=number, total=total_results, page=api_page, api_params=api_params)


# Σελίδα προβολής σχετικών πληροφοριών με την εφαρμογή
@app.route("/about/")
def about():
    return render_template("about.html")
